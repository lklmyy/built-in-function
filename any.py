#!/usr/bin/python
# -*- coding: UTF-8 -*-

# ---------------------------------------------------------
# 环境：Pycharm + Python3
# 功能说明：python内置函数any()
# URL：https://docs.python.org/3.6/library/functions.html?highlight=any#any
# 函数描述：any()函数用于判断给定的迭代器iterator是否全部为False,如果全部为False则返回
#           False,只要有一个不为False则返回True。False的类型可以是数字0、空、False。
# 语法：any(items)
# 参数：items--元组或列表
# 返回值：如果都为空、0、false，则返回false，如果不都为空、0、false，则返回true。
# 注意事项：该函数限Python2.5以上版本使用
# 作者: lkl
# 联系：kailiangmyy@gmail.com
# 创建时间：2018-12-25
# 修改时间：2018-12-25
# ---------------------------------------------------------

"""
内置函数：
any(iterable)
Return True if any element of the iterable is true. If the iterable is empty, return False. Equivalent to:

def any(iterable):
    for element in iterable:
        if element:
            return True
    return False
"""

if __name__ == '__main__':
    # 测试用例
    # 列表不存在反例(空、0、False)
    case_1 = ['google', 'tencent', 1, True]
    print(any(case_1))  # print true

    # 列表存在反例
    case_2 = ['google', 'tencent', 0, False]
    print(any(case_2))  # print true

    # 列表存在空格
    case_3 = (' ', 'tencent', 1, True)
    print(any(case_3))  # print true

    # 列表为空
    case_4 = []
    print(any(case_4))  # print False

    # 列表全为反例
    case_6 = [0, '', False]
    print(any(case_6))  # print False

    # 元组为全为正例
    case_7 = (' ', 'tencent', 1, True)
    print(any(case_7))  # print true

    # 元组存在负例
    case_8 = (' ', 0, False)
    print(any(case_8))  # print true

    # 元组为空
    case_9 = ()
    print(any(case_9))  # print False
