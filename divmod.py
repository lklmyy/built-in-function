#!/usr/bin/python
# -*- coding: UTF-8 -*-

# ---------------------------------------------------------
# 环境：Pycharm + Python3
# 功能说明：python内置函数any()
# 函数描述：除法操作，divmod()包含两个参数，被除数和除数，返回一个包含两个元素的元组，分别为商和余数(a // b, a % b)。
# 作者: lkl
# 联系：kailiangmyy@gmail.com
# 创建时间：2018-12-27
# 修改时间：2018-12-27
# ---------------------------------------------------------


def divmod_demo():
    print(divmod(3, 2))
    print(divmod(1 + 2j, 1 + 0.5j))
    print(divmod(8, 2))

def input_demo():
    pass


if __name__ == '__main__':
    divmod_demo()
